# radeon_imac

This is a patched radeon driver for the Apple iMac 10.1

These system are being special in how they configure their GPU, which leads to the following issue on Linux, where the displays shows 4 time (barely) the desktop.
![iMac running linux Mint with garbled display](/doc/display_issue.jpg "Original Driver Display Issue")

This can be worked around by booting with the ```nomodeset``` kernel argument, but then graphic acceleration is disabled, which prevents most modern app from running smoothly.

This driver provides a fix to that issue, by preventing the kernel from configuring the GPU encoders / transmitters states (the system boots in a working state).

This contains the patched driver in a format ready to use with DKMS, so that each new kernel installed can use the patched module.

# Limitations 

 - Sleep is not supported. The display won't wake up from sleep. To avoid triggering that issue, sleep mode can be disabled in that system with the following command : 
```
$ sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
```
 - Sometimes the display is blurred after turning on. This might be caused by a race condition somewhere. This can be fixed with the follwing command : 
```
$ xrandr --output LVDS --off && xrandr --output LVDS -auto
```

# Installation

Navigate to the radeon_imac folder.

Blacklist original radeon module
```
$ echo "blacklist radeon" | sudo tee -a /etc/modprobe.d/blacklist.conf
```

Configure DKMS
```
$ sudo cp -R . /usr/src/radeon_imac-1.0
$ sudo dkms add -m radeon_imac -v 1.0
$ sudo dkms build -m radeon_imac -v 1.0
$ sudo dkms install -m radeon_imac -v 1.0
```

Reboot
```
sudo reboot
```

# More infos 

This is based on the radeon module in v6.2 of the Linux Kernel. The patch I created is included. 
Also included are my notes, and a comparison of the GPU registers between a Ubuntu 6.2 kernel booted normally, or with the ```nomodeset``` arguments, created with the ````avivotool``` tool . 
I did identify a few registers that differ, and was able to manually force them to the correct value, but the results were no reliable (about 50% success rate).

Also it is a good idea to replace the grub bootloader by refind on these machines, as grub does NOT show up.
```
$ sudo apt install refind
```


